package tdt.timxethainguyen.data;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class BookmarkAdapter extends RecyclerView.Adapter<tdt.timxethainguyen.data.RecyclerViewHolder> {
    private ArrayList<tdt.timxethainguyen.support.ItemBookmark> listData;
    Context mContext;
    tdt.timxethainguyen.data.BookmarkDataHelper bookmarkDataHelper;
    public BookmarkAdapter(Context mContext, ArrayList<tdt.timxethainguyen.support.ItemBookmark> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<tdt.timxethainguyen.support.ItemBookmark> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public tdt.timxethainguyen.data.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                          int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(tdt.example.timxethainguyen.R.layout.bookmark_item, viewGroup, false);
        return new tdt.timxethainguyen.data.RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final tdt.timxethainguyen.data.RecyclerViewHolder viewHolder, final int position) {
        viewHolder.tvTenTuyen.setText(listData.get(position).getBMTenTuyen());
        String loaixe = listData.get(position).getBMLoaiXe();
        switch (listData.get(position).getBMLoaiXe()) {
            case "coach":
                loaixe = "Tuyến xe khách: ";
                break;
            case "bus":
                loaixe = "Tuyến xe buýt: ";
                break;
        }
        viewHolder.tvLoaiXe.setText(loaixe);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listData.get(position).getBMLoaiXe()) {
                    case "coach":
                        Intent i = new Intent(mContext, tdt.timxethainguyen.main.CoachItem.class);
                        i.putExtra("matuyen", listData.get(position).getBMMaTuyen());
                        v.getContext().startActivity(i);
                        break;
                    case "bus":
                        Intent i2 = new Intent(mContext, tdt.timxethainguyen.main.BusItem.class);
                        i2.putExtra("matuyen", listData.get(position).getBMMaTuyen());
                        v.getContext().startActivity(i2);
                        break;
                }
            }
        });
        viewHolder.imgXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookmarkDataHelper = new tdt.timxethainguyen.data.BookmarkDataHelper(mContext);
                bookmarkDataHelper.deleteItem("ma_tuyen = '"+listData.get(position).getBMMaTuyen()+"'");
                Fragment fragment = new tdt.timxethainguyen.main.BookmarkFragment();
                FragmentManager fragmentManager = ((tdt.timxethainguyen.main.MainActivity) viewHolder.itemView.getContext()).getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(tdt.example.timxethainguyen.R.id.content_frame, fragment)
                        .commit();
                Toast.makeText(mContext,"Đã xóa",Toast.LENGTH_SHORT).show();
            }
        });
    }
}

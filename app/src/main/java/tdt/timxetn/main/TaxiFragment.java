package tdt.timxetn.main;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

import tdt.timxetn.R;
import tdt.timxetn.data.DatabaseHelper;
import tdt.timxetn.data.TaxiAdapter;
import tdt.timxetn.support.ItemCoach;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class TaxiFragment extends Fragment {
    View mView;
    DatabaseHelper myDbHelper;
    Context mContext;
    ArrayList<ItemCoach> arrayList = new ArrayList<>();
    RecyclerView recyclerView;
    TaxiAdapter taxiAdapter;
    LinearLayoutManager layoutManager;
    String maTinh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.taxi_layout,container, false);
        mContext = this.getActivity().getApplicationContext();
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_taxi);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        taxiAdapter = new TaxiAdapter(mContext, arrayList);
        recyclerView.setAdapter(taxiAdapter);
        getData();
        return mView;
    }
    private void getData() {
        myDbHelper = new DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        ItemCoach item = null;
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM taxi", null);
        if(c.moveToFirst()){
            do{
                String mtx = c.getString(c.getColumnIndex("ma_taxi"));
                String ttx = c.getString(c.getColumnIndex("ten_taxi"));
                String sdt = c.getString(c.getColumnIndex("so_dt"));
                item = new ItemCoach(mtx, ttx, sdt);
                arrayList.add(item);
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
    }
}

package tdt.timxethainguyen.main;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import tdt.timxethainguyen.data.BookmarkDataHelper;
import tdt.timxethainguyen.support.ItemBookmark;
import tdt.timxethainguyen.support.ItemCoach;

/**
 * Created by ITACHI on 22/05/2016.
 */
public class BusItem extends AppCompatActivity implements View.OnClickListener{
    Context mContext;
    tdt.timxethainguyen.data.DatabaseHelper myDbHelper;
    BookmarkDataHelper bookmarkDataHelper;
    TextView tvTenTuyen, tvLoTrinh, tvDiemDung;
    ImageButton btnDanhDau, btnLoTrinh;
    String maTuyen, tenTuyen, loTrinh, diemDung;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(tdt.example.timxethainguyen.R.layout.bus_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this.getApplicationContext();
        tvTenTuyen = (TextView) findViewById(tdt.example.timxethainguyen.R.id.tv_ttuyen);
        tvLoTrinh = (TextView) findViewById(tdt.example.timxethainguyen.R.id.tv_lotrinh);
        tvDiemDung = (TextView) findViewById(tdt.example.timxethainguyen.R.id.tv_diemdung);
        btnDanhDau = (ImageButton) findViewById(tdt.example.timxethainguyen.R.id.btn_danhdau);
        btnDanhDau.setOnClickListener(this);
        btnLoTrinh = (ImageButton) findViewById(tdt.example.timxethainguyen.R.id.btn_lotrinh);
        btnLoTrinh.setOnClickListener(this);
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        maTuyen = bundle.getString("matuyen");
        myDbHelper = new tdt.timxethainguyen.data.DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        ItemCoach item = null;
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM xe_bus WHERE ma_tuyen = '"+maTuyen+"'", null);
        if(c.moveToFirst()){
            do{
                tenTuyen = c.getString(c.getColumnIndex("ten_tuyen"));
                loTrinh = c.getString(c.getColumnIndex("lo_trinh"));
                diemDung = c.getString(c.getColumnIndex("diem_dung"));
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
        tvTenTuyen.setText(tenTuyen+"\n"+loTrinh);
        tvDiemDung.setText(diemDung);
        bookmarkDataHelper = new BookmarkDataHelper(mContext);
        if (bookmarkDataHelper.isItemExist(maTuyen) == true)
            btnDanhDau.setBackgroundResource(tdt.example.timxethainguyen.R.drawable.star_icon);
        else
            btnDanhDau.setBackgroundResource(tdt.example.timxethainguyen.R.drawable.un_star_icon);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case tdt.example.timxethainguyen.R.id.btn_danhdau:
                ItemBookmark itemBookmark = null;
                if (bookmarkDataHelper.isItemExist(maTuyen) == false) {
                    itemBookmark = new ItemBookmark(maTuyen, tenTuyen+"\n"+loTrinh, "bus");
                    bookmarkDataHelper.insertItem(itemBookmark);
                    btnDanhDau.setBackgroundResource(tdt.example.timxethainguyen.R.drawable.star_icon);
                    Toast.makeText(this,"Đã thêm đánh dấu",Toast.LENGTH_SHORT).show();
                }
                else {
                    bookmarkDataHelper.deleteItem(maTuyen);
                    btnDanhDau.setBackgroundResource(tdt.example.timxethainguyen.R.drawable.un_star_icon);
                    Toast.makeText(this,"Đã bỏ đánh dấu",Toast.LENGTH_SHORT).show();
                }
                break;
            case tdt.example.timxethainguyen.R.id.btn_lotrinh:
                Toast.makeText(this,"Chưa cập nhật",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

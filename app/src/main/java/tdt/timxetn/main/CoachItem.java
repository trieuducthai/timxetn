package tdt.timxetn.main;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import tdt.timxetn.R;
import tdt.timxetn.data.BookmarkDataHelper;
import tdt.timxetn.data.DatabaseHelper;
import tdt.timxetn.map.MapsActivity;
import tdt.timxetn.support.ItemBookmark;

/**
 * Created by ITACHI on 22/05/2016.
 */
public class CoachItem extends AppCompatActivity implements View.OnClickListener{
    Context mContext;
    DatabaseHelper myDbHelper;
    BookmarkDataHelper bookmarkDataHelper;
    private TextView tvTenTuyen, tvSoXe, tvDVVT, tvGioKH, tvGiaVe, tvSoDT, tvLoTrinh, tvCuLy;
    private ImageButton btnDanhDau, btnLoTrinh;
    private String maTuyen, tenBen, tenTinh, maBen, soXe, dvVT, gioKH, soDT, loTrinh, maTinh;
    private int giaVe, cuLy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coach_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this.getApplicationContext();
        tvTenTuyen = (TextView) findViewById(R.id.tv_ttuyen);
        tvSoXe = (TextView) findViewById(R.id.tv_soxe);
        tvDVVT = (TextView) findViewById(R.id.tv_dvvt);
        tvGioKH = (TextView) findViewById(R.id.tv_giokhoihanh);
        tvGiaVe = (TextView) findViewById(R.id.tv_giave);
        tvSoDT = (TextView) findViewById(R.id.tv_sdt);
        tvLoTrinh = (TextView) findViewById(R.id.tv_lotrinh);
        tvCuLy = (TextView) findViewById(R.id.tv_culy);
        btnDanhDau = (ImageButton) findViewById(R.id.btn_danhdau);
        btnDanhDau.setOnClickListener(this);
        btnLoTrinh = (ImageButton) findViewById(R.id.btn_lotrinh);
        btnLoTrinh.setOnClickListener(this);
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        maTuyen = bundle.getString("matuyen");
        myDbHelper = new DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM xe_khach WHERE ma_tuyen = '"+maTuyen+"'", null);
        if(c.moveToFirst()){
            do{;
                maBen = c.getString(c.getColumnIndex("ma_ben"));
                loTrinh = c.getString(c.getColumnIndex("lo_trinh"));
                soXe = c.getString(c.getColumnIndex("so_xe"));
                soDT = c.getString(c.getColumnIndex("so_dt"));
                dvVT = c.getString(c.getColumnIndex("dv_van_tai"));
                giaVe = c.getInt(c.getColumnIndex("gia_ve"));
                gioKH = c.getString(c.getColumnIndex("gio_khoi_hanh"));
            }
            while(c.moveToNext());
        }
        c.close();
        Cursor c2 = db.rawQuery("SELECT * FROM ben_xe WHERE ma_ben = '"+maBen+"'", null);
        if(c2.moveToFirst()){
            do{
                maTinh = c2.getString(c2.getColumnIndex("ma_tinh"));
                cuLy = c2.getInt(c2.getColumnIndex("cu_ly"));
                tenBen = c2.getString(c2.getColumnIndex("ten_ben"));
            }
            while(c2.moveToNext());
        }
        c2.close();
        Cursor c3 = db.rawQuery("SELECT ten_tinh FROM tinh WHERE ma_tinh = '"+maTinh+"'", null);
        if(c3.moveToFirst()) {
            do {
                tenTinh = c3.getString(c3.getColumnIndex("ten_tinh"));
            }
            while (c3.moveToNext());
        }
        c3.close();
        db.close();

        String tentuyen = "BX. Thái Nguyên -\n"
                +tenBen
                +" ("+tenTinh+")";
        tvTenTuyen.setText(tentuyen);
        tvSoXe.setText(soXe);
        tvDVVT.setText(dvVT);
        tvGioKH.setText(gioKH);
        tvGiaVe.setText(String.valueOf(giaVe));
        tvSoDT.setText(soDT);
        tvLoTrinh.setText(loTrinh);
        tvCuLy.setText(String.valueOf(cuLy)+" Km");
        bookmarkDataHelper = new BookmarkDataHelper(mContext);
        if (bookmarkDataHelper.isItemExist(maTuyen) == true)
            btnDanhDau.setBackgroundResource(R.drawable.star_icon);
        else
            btnDanhDau.setBackgroundResource(R.drawable.un_star_icon);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_danhdau:
                ItemBookmark itemBookmark = null;
                if (bookmarkDataHelper.isItemExist(maTuyen) == false) {
                    itemBookmark = new ItemBookmark(maTuyen, "Thái Nguyên - " + tenBen + " (" + tenTinh + ")", "coach");
                    bookmarkDataHelper.insertItem(itemBookmark);
                    btnDanhDau.setBackgroundResource(R.drawable.star_icon);
                    Toast.makeText(this,"Đã thêm đánh dấu",Toast.LENGTH_SHORT).show();
                }
                else {
                    bookmarkDataHelper.deleteItem(maTuyen);
                    btnDanhDau.setBackgroundResource(R.drawable.un_star_icon);
                    Toast.makeText(this,"Đã bỏ đánh dấu",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_lotrinh:
                Intent intent = new Intent(CoachItem.this, MapsActivity.class);
                intent.putExtra("tentinh", tenTinh);
                intent.putExtra("tenben", tenBen);
                startActivity(intent);
                break;
        }
    }
}

package tdt.timxethainguyen.support;

/**
 * Created by ITACHI on 25/05/2016.
 */
public class ItemBookmark {
    private String bmMaTuyen;
    private String bmLoaiXe;
    private String bmTenTuyen;

    public ItemBookmark() {}

    public ItemBookmark (String bnMaTuyen, String bmTenTuyen, String bmLoaiXe){
        this.bmMaTuyen = bnMaTuyen;
        this.bmLoaiXe = bmLoaiXe;
        this.bmTenTuyen = bmTenTuyen;
    }

    public String getBMMaTuyen() {
        return bmMaTuyen;
    }

    public void setBmMaTuyen(String bmMaTuyen) {
        this.bmMaTuyen = bmMaTuyen;
    }

    public String getBMTenTuyen() {
        return bmTenTuyen;
    }

    public void setBmTenTuyen(String bmTenTuyen) {
        this.bmTenTuyen = bmTenTuyen;
    }

    public String getBMLoaiXe() {
        return bmLoaiXe;
    }

    public void setBmLoaiXe(String bmLoaiXe) {
        this.bmLoaiXe = bmLoaiXe;
    }
}

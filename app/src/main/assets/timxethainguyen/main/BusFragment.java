package tdt.timxethainguyen.main;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class BusFragment extends Fragment {
    View mView;
    tdt.timxethainguyen.data.DatabaseHelper myDbHelper;
    Context mContext;
    ArrayList<tdt.timxethainguyen.support.ItemCoach> arrayList = new ArrayList<>();
    RecyclerView recyclerView;
    tdt.timxethainguyen.data.BusAdapter busAdapter;
    LinearLayoutManager layoutManager;
    String maTinh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(tdt.example.timxethainguyen.R.layout.bus_layout,container, false);
        mContext = this.getActivity().getApplicationContext();
        recyclerView = (RecyclerView) mView.findViewById(tdt.example.timxethainguyen.R.id.recycler_bus);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        busAdapter = new tdt.timxethainguyen.data.BusAdapter(mContext, arrayList);
        recyclerView.setAdapter(busAdapter);
        getData();
        return mView;
    }
    private void getData() {
        myDbHelper = new tdt.timxethainguyen.data.DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        tdt.timxethainguyen.support.ItemCoach item = null;
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM xe_bus", null);
        if(c.moveToFirst()){
            do{
                String mt = c.getString(c.getColumnIndex("ma_tuyen"));
                String tt = c.getString(c.getColumnIndex("ten_tuyen"));
                String lt = c.getString(c.getColumnIndex("lo_trinh"));
                String dd = c.getString(c.getColumnIndex("diem_dung"));
                item = new tdt.timxethainguyen.support.ItemCoach(mt, tt, lt, dd);
                arrayList.add(item);
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
    }
}

package tdt.timxetn.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import tdt.timxetn.R;
import tdt.timxetn.main.CoachItem;
import tdt.timxetn.support.ItemCoach;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class ViewCoachAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<ItemCoach> listData;
    Context mContext;
    public ViewCoachAdapter(Context mContext, ArrayList<ItemCoach> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<ItemCoach> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                 int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.view_coach_item, viewGroup, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder viewHolder, final int position) {
        viewHolder.tvGioKH.setText(String.valueOf(listData.get(position).getGioKH()));
        String tentuyen = "BX. Thái Nguyên -\n"
                +listData.get(position).getTenBen()
                +" ("+listData.get(position).getTenTinh()+")";
        viewHolder.tvTenTuyen.setText(tentuyen);
        viewHolder.tvDVVT.setText(listData.get(position).getDVVT());
        viewHolder.tvGiaVe.setText(String.valueOf(listData.get(position).getGiaVe())+"đ");
        viewHolder.imgLienHe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (listData.get(position).getSoDT().length() < 6) {
                        Toast.makeText(v.getContext(), "Chưa có", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + listData.get(position).getSoDT()));
                        v.getContext().startActivity(callIntent);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Toast.makeText(v.getContext(), "Chưa có", Toast.LENGTH_SHORT).show();
                }
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, CoachItem.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("matuyen",listData.get(position).getMaTuyen());
                    intent.putExtras(bundle);
                    v.getContext().startActivity(intent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

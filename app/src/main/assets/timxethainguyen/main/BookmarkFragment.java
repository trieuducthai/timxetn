package tdt.timxethainguyen.main;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tdt.example.timxethainguyen.R;
import tdt.timxethainguyen.data.BookmarkAdapter;
import tdt.timxethainguyen.data.BookmarkDataHelper;
import tdt.timxethainguyen.support.ItemBookmark;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class BookmarkFragment extends Fragment {
    View mView;
    BookmarkDataHelper bmDbHelper;
    Context mContext;
    ArrayList<ItemBookmark> myArrayList = new ArrayList<>();
    RecyclerView recyclerView;
    BookmarkAdapter bookmarkAdapter;
    LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.bookmark_layout,container, false);
        mContext = this.getActivity().getApplicationContext();
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_bookmark);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        bookmarkAdapter = new BookmarkAdapter(mContext, myArrayList);
        recyclerView.setAdapter(bookmarkAdapter );
        getData();
        bookmarkAdapter.updateList(myArrayList);
        return mView;
    }
    private void getData() {
        bmDbHelper = new BookmarkDataHelper(mContext);
        ItemBookmark item = null;
        SQLiteDatabase db = bmDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tb_bookmark", null);
        if(c.moveToFirst()){
            do{
                String mt = c.getString(c.getColumnIndex("ma_tuyen"));
                String tt = c.getString(c.getColumnIndex("ten_tuyen"));
                String lx = c.getString(c.getColumnIndex("loai_xe"));
                item = new ItemBookmark(mt, tt, lx);
                myArrayList.add(item);
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
    }
}

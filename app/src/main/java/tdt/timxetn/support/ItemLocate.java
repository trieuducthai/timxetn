package tdt.timxetn.support;

/**
 * Created by ITACHI on 25/05/2016.
 */
public class ItemLocate {
    private String startLatLng;
    private String endLatLng;

    public ItemLocate() {}

    public ItemLocate(String start, String end){
        this.startLatLng = start;
        this.endLatLng = end;
    }

    public String getStartLatLng() {
        return startLatLng;
    }

    public void setStartLatLng(String start) {
        this.startLatLng = start;
    }

    public String getEndLatLng() {
        return endLatLng;
    }

    public void setEndLatLng(String end) {
        this.endLatLng = end;
    }
}

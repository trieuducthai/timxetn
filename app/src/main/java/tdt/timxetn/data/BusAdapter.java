package tdt.timxetn.data;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tdt.timxetn.R;
import tdt.timxetn.main.BusItem;
import tdt.timxetn.support.ItemCoach;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class BusAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<ItemCoach> listData;
    Context mContext;
    public BusAdapter(Context mContext, ArrayList<ItemCoach> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<ItemCoach> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                 int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.bus_item, viewGroup, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder viewHolder, final int position) {
        String tentuyen = listData.get(position).getTenTuyen();
        if (tentuyen != null) {
            viewHolder.tvTenTuyen.setText(tentuyen);
        }
        else
            viewHolder.tvTenTuyen.setText("Đang cập nhật");
        viewHolder.tvLoTrinh.setText(listData.get(position).getLoTrinh());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, BusItem.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("matuyen",listData.get(position).getMaTuyen());
                    intent.putExtras(bundle);
                    v.getContext().startActivity(intent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

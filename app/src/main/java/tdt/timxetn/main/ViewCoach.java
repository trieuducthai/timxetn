package tdt.timxetn.main;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import tdt.timxetn.R;
import tdt.timxetn.data.DatabaseHelper;
import tdt.timxetn.data.ViewCoachAdapter;
import tdt.timxetn.support.ItemCoach;

/**
 * Created by ITACHI on 22/05/2016.
 */
public class ViewCoach extends AppCompatActivity {
    DatabaseHelper myDbHelper;
    Context mContext;
    ArrayList<ItemCoach> arrayList = new ArrayList<>();
    RecyclerView recyclerView;
    ViewCoachAdapter viewCoachAdapter;
    LinearLayoutManager layoutManager;
    String maBen, tenBen, tenTinh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this.getApplicationContext();
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        viewCoachAdapter = new ViewCoachAdapter(mContext, arrayList);
        recyclerView.setAdapter(viewCoachAdapter);
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            maBen = bundle.getString("maben");
            tenBen = bundle.getString("tenben");
            tenTinh = bundle.getString("tentinh");
        } else
            Toast.makeText(mContext,"Không có dữ liệu",Toast.LENGTH_SHORT);

        myDbHelper = new DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        ItemCoach item = null;
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM xe_khach WHERE ma_ben = "+"'"+maBen+"'", null);
        if(c.moveToFirst()){
            do{
                String mt = c.getString(c.getColumnIndex("ma_tuyen"));
                String lt = c.getString(c.getColumnIndex("lo_trinh"));
                String sx = c.getString(c.getColumnIndex("so_xe"));
                String sdt = c.getString(c.getColumnIndex("so_dt"));
                String dv = c.getString(c.getColumnIndex("dv_van_tai"));
                int gv = c.getInt(c.getColumnIndex("gia_ve"));
                String gkh = c.getString(c.getColumnIndex("gio_khoi_hanh"));
                item = new ItemCoach(mt, tenTinh, tenBen, lt, sx, sdt, dv, gv, gkh);
                arrayList.add(item);
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
    }
}

package tdt.timxethainguyen.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class ViewCoachAdapter extends RecyclerView.Adapter<tdt.timxethainguyen.data.RecyclerViewHolder> {
    private ArrayList<tdt.timxethainguyen.support.ItemCoach> listData;
    Context mContext;
    public ViewCoachAdapter(Context mContext, ArrayList<tdt.timxethainguyen.support.ItemCoach> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<tdt.timxethainguyen.support.ItemCoach> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public tdt.timxethainguyen.data.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                          int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(tdt.example.timxethainguyen.R.layout.view_coach_item, viewGroup, false);
        return new tdt.timxethainguyen.data.RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(tdt.timxethainguyen.data.RecyclerViewHolder viewHolder, final int position) {
        viewHolder.tvGioKH.setText(String.valueOf(listData.get(position).getGioKH()));
        String tentuyen = "BX. Thái Nguyên -\n"
                +listData.get(position).getTenBen()
                +" ("+listData.get(position).getTenTinh()+")";
        viewHolder.tvTenTuyen.setText(tentuyen);
        viewHolder.tvDVVT.setText(listData.get(position).getDVVT());
        viewHolder.tvGiaVe.setText(String.valueOf(listData.get(position).getGiaVe())+"đ");
        viewHolder.imgLienHe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listData.get(position).getSoDT().isEmpty()) {
                    Toast.makeText(v.getContext(),"Chưa có",Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + listData.get(position).getSoDT()));
                    v.getContext().startActivity(callIntent);
                }
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, tdt.timxethainguyen.main.CoachItem.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("matuyen",listData.get(position).getMaTuyen());
                    intent.putExtras(bundle);
                    v.getContext().startActivity(intent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

package tdt.timxetn.main;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tdt.timxetn.R;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class CoachFragment extends Fragment {
    View mView;
    Context mContext;
    static final int PICK_PROVINE_REQUEST = 1;
    static final int PICK_STATION_REQUEST = 2;
    static final int PICK_DATA_REQUEST = 3;
    private TextView tvProvince;
    private TextView tvStation;
    private CardView cvProvine;
    private CardView cvStation;
    private Button btnSearch;
    private String mTinh, tTinh, mBen, tBen;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.coach_layout, container, false);
        mContext = this.getActivity().getApplicationContext();
        cvProvine = (CardView) mView.findViewById(R.id.provine_card_view);
        cvStation = (CardView) mView.findViewById(R.id.station_card_view);
        tvProvince = (TextView) mView.findViewById(R.id.tv_selectTinh);
        tvStation = (TextView) mView.findViewById(R.id.tv_selectBen);
        btnSearch = (Button) mView.findViewById(R.id.btn_search);
        pickProvine();
        return mView;
    }

    private void pickProvine() {
        cvProvine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickProvineIntent = new Intent(mContext, Province.class);
                startActivityForResult(pickProvineIntent, PICK_PROVINE_REQUEST);
            }
        });
    }

    private void pickStation() {
        cvStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickStationIntent = new Intent(mContext, CoachStation.class);
                pickStationIntent.putExtra("matinh", mTinh);
                startActivityForResult(pickStationIntent, PICK_STATION_REQUEST);
            }
        });
    }

    private void getData() {
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getDataIntent = new Intent(mContext, ViewCoach.class);
                Bundle bundle = new Bundle();
                bundle.putString("maben", mBen);
                bundle.putString("tenben", tBen);
                bundle.putString("matinh", mTinh);
                bundle.putString("tentinh", tTinh);
                getDataIntent.putExtras(bundle);
                startActivityForResult(getDataIntent, PICK_DATA_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_PROVINE_REQUEST:
                if(resultCode == this.getActivity().RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    mTinh = bundle.getString("matinh");
                    tTinh = bundle.getString("tentinh");
                    tvProvince.setText(tTinh);
                    tvStation.setEnabled(true);
                    cvStation.setClickable(true);
                    pickStation();
                }
                break;
            case PICK_STATION_REQUEST:
                if(resultCode == this.getActivity().RESULT_OK) {
                    Bundle bundle2 = data.getExtras();
                    mBen = bundle2.getString("maben");
                    tBen = bundle2.getString("tenben");
                    tvStation.setText(tBen);
                    btnSearch.setClickable(true);
                    getData();
                }
                break;
            case PICK_DATA_REQUEST:
                break;
        }
    }
}


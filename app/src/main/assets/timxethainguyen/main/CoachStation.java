package tdt.timxethainguyen.main;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import tdt.example.timxethainguyen.R;
import tdt.timxethainguyen.data.CoachStationAdapter;
import tdt.timxethainguyen.support.ItemCoach;

/**
 * Created by ITACHI on 22/05/2016.
 */
public class CoachStation extends AppCompatActivity {
    tdt.timxethainguyen.data.DatabaseHelper myDbHelper;
    Context mContext;
    ArrayList<ItemCoach> arrayList = new ArrayList<>();
    RecyclerView recyclerView;
    CoachStationAdapter stationAdapter;
    LinearLayoutManager layoutManager;
    String maTinh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this.getApplicationContext();
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        stationAdapter = new CoachStationAdapter(mContext, arrayList);
        recyclerView.setAdapter(stationAdapter);
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            maTinh = bundle.getString("matinh");
        } else
            Toast.makeText(mContext,"Không có dữ liệu",Toast.LENGTH_SHORT);

        myDbHelper = new tdt.timxethainguyen.data.DatabaseHelper(mContext);
        try {
            myDbHelper.createDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDatabase();
        }catch(SQLException sqle) {
            throw sqle;
        }
        ItemCoach item = null;
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM ben_xe WHERE ma_tinh = "+"'"+maTinh+"'", null);
        if(c.moveToFirst()){
            do{
                String mb = c.getString(c.getColumnIndex("ma_ben"));
                String tb = c.getString(c.getColumnIndex("ten_ben"));
                int cl = c.getInt(c.getColumnIndex("cu_ly"));
                item = new ItemCoach(mb, tb, maTinh, cl);
                arrayList.add(item);
            }
            while(c.moveToNext());
        }
        c.close();
        db.close();
    }
}

package tdt.timxethainguyen.support;

/**
 * Created by ITACHI on 17/05/2016.
 */

public class ItemCoach {
    private String maTinh;
    private String tenTinh;
    private String maBen;
    private String tenBen;
    private int cuLy;
    private String maTuyen;
    private String loTrinh;
    private String soXe;
    private String soDT;
    private String dvVT;
    private int giaVe;
    private String gioKH;
    private String tenTuyen;
    private String diemDung;
    private String tenTaxi;
    private String maTaxi;

    public ItemCoach() {
    }

    public ItemCoach(String matinh, String tentinh) {
        this.maTinh = matinh;
        this.tenTinh = tentinh;
    }

    public ItemCoach(String mataxi, String tentaxi, String sodt) {
        this.maTaxi = mataxi;
        this.tenTaxi = tentaxi;
        this.soDT = sodt;
    }

    public ItemCoach(String maben, String tenben, String matinh, int culy) {
        this.maBen = maben;
        this.tenBen = tenben;
        this.maTinh = matinh;
        this.cuLy = culy;
    }

    public ItemCoach(String matuyen, String tentinh, String tenben, String lotrinh, String soxe,
                     String sodt, String dvvt, int giave, String giokh) {
        this.maTuyen = matuyen;
        this.tenTinh = tentinh;
        this.tenBen = tenben;
        this.loTrinh = lotrinh;
        this.soXe = soxe;
        this.soDT = sodt;
        this.dvVT = dvvt;
        this.giaVe = giave;
        this.gioKH = giokh;
    }

    public ItemCoach(String matuyen, String tentuyen, String lotrinh, String diemdung) {
        this.maTuyen = matuyen;
        this.tenTuyen = tentuyen;
        this.loTrinh = lotrinh;
        this.diemDung = diemdung;
    }

    public String getMaTinh() {
        return maTinh;
    }

    public void setMaTinh (String matinh) {
        this.maTinh = matinh;
    }

    public String getTenTinh() {
        return tenTinh;
    }

    public void setTenTinh (String tentinh) {
        this.tenTinh = tentinh;
    }

    public String getMaBen() {
        return maBen;
    }

    public tdt.timxethainguyen.support.ItemCoach setMaBen (String maben) {
        this.maBen = maben;
        return this;
    }

    public String getTenBen() {
        return tenBen;
    }

    public void setTenBen (String tenben) {
        this.tenBen = tenben;
    }

    public int getCuLy() {
        return cuLy;
    }

    public void setCuLy (int culy) {
        this.cuLy = culy;
    }

    public String getMaTuyen() {
        return maTuyen;
    }

    public void setMaTuyen (String matuyen) {
        this.maTuyen = matuyen;
    }

    public String getLoTrinh() {
        return loTrinh;
    }

    public void setLoTrinh (String lotrinh) {
        this.loTrinh = lotrinh;
    }

    public String getSoXe() {
        return soXe;
    }

    public void setSoXe (String soxe) {
        this.soXe = soxe;
    }

    public String getSoDT() {
        return soDT;
    }

    public void setSoDT (String sodt) {
        this.soDT = sodt;
    }

    public String getDVVT() {
        return dvVT;
    }

    public void setDVVT (String dvvt) {
        this.dvVT = dvvt;
    }

    public int getGiaVe() {
        return giaVe;
    }

    public void setGiaVe (int giave) {
        this.cuLy = giave;
    }

    public String getGioKH() {
        return gioKH;
    }

    public void setGioKH (String giokh) {
        this.gioKH = giokh;
    }

    public String getTenTuyen() { return tenTuyen; }

    public void setTenTuyen(String tentuyen) { this.tenTuyen = tentuyen; }

    public String getDiemDung() { return diemDung; }

    public void setDiemDung(String diemdung) { this.diemDung = diemdung; }

    public String getMaTaxi() { return maTaxi; }

    public void setMaTaxi(String mataxi) { this.maTaxi = mataxi; }

    public String getTenTaxi() { return tenTaxi; }

    public void setTenTaxi(String tentaxi) { this.tenTaxi = tentaxi; }

//    public String toJSON() {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put(Var.KEY_ID, getId());
//            jsonObject.put(Var.KEY_NAME, getName());
//            jsonObject.put(Var.KEY_PHONE, getPhone());
//            jsonObject.put(Var.KEY_BEGIN_DATE, getBeginDate());
//            jsonObject.put(Var.KEY_END_DATE, getEndDate());
//
//            return jsonObject.toString();
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return "";
//        }
//    }
}

package tdt.timxethainguyen.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tdt.timxethainguyen.support.ItemCoach;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class TaxiAdapter extends RecyclerView.Adapter<tdt.timxethainguyen.data.RecyclerViewHolder> {
    private ArrayList<ItemCoach> listData;
    Context mContext;
    public TaxiAdapter(Context mContext, ArrayList<ItemCoach> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<ItemCoach> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public tdt.timxethainguyen.data.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                          int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(tdt.example.timxethainguyen.R.layout.taxi_item, viewGroup, false);
        return new tdt.timxethainguyen.data.RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(tdt.timxethainguyen.data.RecyclerViewHolder viewHolder, final int position) {
        viewHolder.tvTenTaxi.setText(listData.get(position).getTenTaxi());
        viewHolder.tvLienHe.setText(listData.get(position).getSoDT());
        viewHolder.tvLienHe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+listData.get(position).getSoDT()));
                v.getContext().startActivity(callIntent);
            }
        });
    }
}

package tdt.timxethainguyen.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ITACHI on 25/05/2016.
 */
public class BookmarkDataHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "bookmark.db";

    public static final String TABLE_BOOKMARK = "tb_bookmark";
    public static final String KEY_MA_TUYEN = "ma_tuyen";
    public static final String KEY_TEN_TUYEN = "ten_tuyen";
    public static final String KEY_LOAI_XE = "loai_xe";

    public static final String CREATE_TABLE_BOOKMARK =
            "CREATE TABLE " + TABLE_BOOKMARK + "(" +
                    KEY_MA_TUYEN + " VARCHAR PRIMARY KEY NOT NULL" +
                    ", " + KEY_TEN_TUYEN + " VARCHAR NULL" +
                    ", " + KEY_LOAI_XE + " VARCHAR NOT NULL" +
                    ")";

    public static final int DATA_VERSION = 1;

    private SQLiteDatabase db;

    public BookmarkDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_BOOKMARK);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void open() {
        try {
            db = getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openRead() {
        try {
            db = getReadableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (db != null && db.isOpen()) {
            try {
                db.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Cursor getAll(String sql) {
        open();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        close();
        return cursor;
    }

    public long insert(String table, ContentValues values) {
        open();
        long index = db.insert(table, null, values);
        close();
        return index;
    }

    public long insertItem(tdt.timxethainguyen.support.ItemBookmark item) {
        return insert(TABLE_BOOKMARK, itemToValues(item));
    }

    public void delete(String table, String where) {
        open();
        db.delete(table, where, null);
        close();
    }

    public void deleteItem(String where) {
        delete(TABLE_BOOKMARK, where);
    }

    private ContentValues itemToValues(tdt.timxethainguyen.support.ItemBookmark item) {
        ContentValues values = new ContentValues();
        values.put(KEY_MA_TUYEN, item.getBMMaTuyen());
        values.put(KEY_TEN_TUYEN, item.getBMTenTuyen());
        values.put(KEY_LOAI_XE, item.getBMLoaiXe());
        return values;
    }

    public boolean isItemExist(String text) {
        openRead();
        String maTuyen = null;
        Boolean test = false;
        Cursor c = db.rawQuery("SELECT ma_tuyen from "+TABLE_BOOKMARK, null);
        if(c.moveToFirst()){
            do{
                maTuyen = c.getString(c.getColumnIndex("ma_tuyen"));
                if (maTuyen == text) {
                    test = true;
                    break;
                }
            }
            while(c.moveToNext());
        }
        c.close();
        close();
        return test;
    }
}

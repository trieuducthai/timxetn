package tdt.timxethainguyen.map;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.Language;
import com.akexorcist.googledirection.constant.TransportMode;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import tdt.example.timxethainguyen.R;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private LatLng origin;
    private LatLng destination;
    private String serverKey = "AIzaSyBwRj9gnSNr0jjeIfZfa8XliKPhS0I-gZs";
    private Button btnRequestDirection;
    private String diemDen, startLatLng, endLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnRequestDirection = (Button) findViewById(R.id.btn_request_direction);
        btnRequestDirection.setOnClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        Bundle bundle = getIntent().getExtras();
        String tenTinh = bundle.getString("tentinh");
        String tenBen = bundle.getString("tenben");
        diemDen = tenBen + ", " + tenTinh;
        if (checkConnect()) {
            origin = new LatLng(21.5909506, 105.8239173);
            destination = getLocationFromAddress(this, diemDen);
            mMap.addMarker(new MarkerOptions().position(origin).title("Bến xe khách Thái Nguyên"));
            mMap.addMarker(new MarkerOptions().position(destination).title(diemDen));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(destination, 10));

            GoogleDirection.withServerKey(serverKey)
                    .from(origin)
                    .to(destination)
                    .language(Language.VIETNAMESE)
                    .transportMode(TransportMode.DRIVING);
        }
        else
            Toast.makeText(this, "Không có kết nối mạng",Toast.LENGTH_SHORT).show();

//            .execute(new DirectionCallback() {
//                @Override
//                public void onDirectionSuccess(Direction direction, String rawBody) {
//                    if (direction.isOK()) {
//                        try {
//                            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
//                            mMap.addPolyline(DirectionConverter.createPolyline(getApplicationContext(), directionPositionList, 5, Color.RED));
//                        }
//                        catch(Exception e) {
//                                e.printStackTrace();
//                        }
//                    }
//
//                }
//
//                @Override
//                public void onDirectionFailure(Throwable t) {
//                    // Do something here
//                }
//            });
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_request_direction && checkConnect()) {

            try {
            String link = "https://maps.googleapis.com/maps/api/directions/json?origin=Bến+xe+khách+Thái+Nguyên&destination="
                    +diemDen.replaceAll(" ", "+");
            String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
            String uri = Uri.encode(link, ALLOWED_URI_CHARS);
            Log.d("URI", uri);

            JSONParser parser = new JSONParser();
                String value = parser.execute(uri).get();
                try {
                    // Getting JSON Array
                    JSONObject json = new JSONObject(value);
                    JSONArray routes = json.getJSONArray("routes");
                    JSONObject c = routes.getJSONObject(0);
                    JSONArray legs = c.getJSONArray("legs");
                    JSONObject c2 = legs.getJSONObject(0);
                    JSONObject end_location = c2.getJSONObject("end_location");
                    String lat_end = end_location.getString("lat");
                    String lng_end = end_location.getString("lng");
                    endLatLng = lat_end+","+lng_end;
                    Log.d("ENDLATLNG", endLatLng);
                    JSONObject start_location = c2.getJSONObject("start_location");
                    String lat_start = start_location.getString("lat");
                    String lng_start = start_location.getString("lng");
                    startLatLng = lat_start+","+lng_start;
                    Log.d("STARTLATLNG", startLatLng);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?saddr="+startLatLng+"&daddr="+endLatLng);
                Log.d("URL",gmmIntentUri.toString());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
            catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this,"Không tìm thấy chỉ đường",Toast.LENGTH_SHORT).show();
            }
        }
    }
    private boolean checkConnect() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }
}

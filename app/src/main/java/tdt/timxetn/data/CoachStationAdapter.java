package tdt.timxetn.data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tdt.timxetn.R;
import tdt.timxetn.support.ItemCoach;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class CoachStationAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<ItemCoach> listData;
    Context mContext;
    public CoachStationAdapter(Context mContext, ArrayList<ItemCoach> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    public void updateList(ArrayList<ItemCoach> data) {
        listData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                 int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.coach_station_item, viewGroup, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder viewHolder, final int position) {
        viewHolder.tvBen.setText(listData.get(position).getTenBen());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("maben", listData.get(position).getMaBen());
                    bundle.putString("tenben", listData.get(position).getTenBen());
                    bundle.putInt("culy", listData.get(position).getCuLy());
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    ((Activity)v.getContext()).setResult(Activity.RESULT_OK, i);
                    ((Activity)v.getContext()).finish();
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

package tdt.timxetn.data;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import tdt.timxetn.R;

/**
 * Created by ITACHI on 24/05/2016.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView tvTinh;
    public TextView tvBen;
    public TextView tvTenTuyen;
    public TextView tvLoTrinh;
    public TextView tvSoXe;
    public ImageView imgLienHe;
    public TextView tvLienHe;
    public TextView tvDVVT;
    public TextView tvGiaVe;
    public TextView tvGioKH;
    public TextView tvTenTaxi;
    public TextView tvLoaiXe;
    public ImageView imgXoa;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        tvTinh = (TextView) itemView.findViewById(R.id.tv_tentinh);
        tvBen = (TextView) itemView.findViewById(R.id.tv_tenben);
        tvGioKH = (TextView) itemView.findViewById(R.id.tv_giokh);
        tvTenTuyen = (TextView) itemView.findViewById(R.id.tv_tentuyen);
        tvDVVT = (TextView) itemView.findViewById(R.id.tv_dvvt);
        tvGiaVe = (TextView) itemView.findViewById(R.id.tv_giave);
        imgLienHe = (ImageView) itemView.findViewById(R.id.img_lienhe);
        tvLoTrinh = (TextView) itemView.findViewById(R.id.tv_lotrinh);
        tvTenTaxi = (TextView) itemView.findViewById(R.id.tv_tentaxi);
        tvLienHe = (TextView) itemView.findViewById(R.id.tv_lienhe);
        tvLoaiXe =(TextView) itemView.findViewById(R.id.tv_loaixe);
        imgXoa = (ImageView) itemView.findViewById(R.id.img_xoa);
    }
}
